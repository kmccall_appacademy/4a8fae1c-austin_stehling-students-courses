class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name, @last_name = first_name, last_name
    @courses = []
  end

  def name
    "#{@first_name} #{@last_name}"
  end

  def enroll(course)
    return if @courses.include?(course)
    raise "error" if has_conflict(course)
    @courses << course
    course.students << self
  end

  def course_load
    c_load = Hash.new(0)
    @courses.each do |course|
      c_load[course.department] += course.credits
    end
    c_load
  end

  def has_conflict(course2)
    @courses.any? do |course|
      course.conflicts_with?(course2)
    end
  end
end
